# Coronavirusmakers Info Hub

Repositorio para el hub de información de coronavirusmakers

La información se encuentra bajo [docs](https://gitlab.com/coronavirusmakers/info/-/blob/master/docs/README.md), y se publica en Gitlab Pages con cada commit utilizando [mkdocs](https://www.mkdocs.org/) y el tema [Material Design](https://squidfunk.github.io/mkdocs-material/getting-started/).
