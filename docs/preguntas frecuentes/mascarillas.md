# Mascarillas
## ¿Qué tipo de mascarilla me recomiendas?
Dada la alta infectividad del SARS-CoVID-2, lo ideal sería que tuviésemos mascarillas de grado P3 (FFP3). Esto no es posible por el desabastecimiento actual, así que depende de tu nivel de exposición se aconseja un nivel de protección u otro. Por defecto, todos debemos usar mascarillas quirúrgicas o en su defecto tapabocas (tapando nariz y boca) de tela o papel. Esto permite aislar de gotas proyectadas por infectados, que tienen la mayor carga vírica. En este sentido unas gafas protectoras o visera aislan los ojos en el mismo sentido. Más importante aún, debemos llevarlas porque si no sabemos que estamos contagiados, esta medida limita el contagio a terceros en gran medida, al contener las gotas proyectadas y la mayoría de aerosoles exhalados. El mensaje es: para todos, siempre, mascarillas quirúrgicas o tapabocas. Tenemos instrucciones para hacer un tapabocas con filtro de café en: 
https://www.coronavirusmakers.org/index.php/es/impresion-3d/mascarillas/36-diy-mascarilla-con-filtro-de-cafe-en-tan-solo-2-minutos

Debemos dejar las máscaras de grado FFP2, N95 y FFP3 para sanitarios en riesgo alto de infección, en tanto que dure el desabastecimiento. Un ambiente altamente hostil es por ejemplo una UCI con infectados en un hospital. En ese ambiente hostil, con gran exposición al virus tanto en gotas proyectadas como en aerosol, los sanitarios deben tener las mayores garantías para evitar un contagio, casi seguro, de no tener las medidas adecuadas. Un sanitario infectado es un sanitario en cuarentena que no podrá asistir a más pacientes. Son héroes, la primera línea de batalla, y así debemos de protegerlos. 

Actualmente en coronavirusmakers.org tratamos de certificar mascarillas por inyección de plástico o goma en los que se usen filtros ya homologados de los presentes en hospitales, para el personal de alto riesgo. Para ello, las autoridades nos piden pasar los protocolos habituales de homologación. Nos puedes ayudar alzando la voz para pedir protocolos abreviados de validación, con todas las garantías, para el contexto de esta pandemia y limitado en el tiempo.
 
## ¿Cuál o cuáles son los materiales válidos para fabricar mascarillas?
Esta es una pregunta sin respuesta fácil. Si hablamos de mascarillas que garanticen un FFP3 para personal en riesgo alto, no podemos más que fabricarlas por inyección en un material apto para que no irrite cara y que sirva a su propósito de estanqueidad garantizada. Además, deberían ser reusables para cambiar filtros homologados, por lo que tienen que poder ser desinfectados con los protocolos establecidos por prevención de riesgos laborales de cada hospital.

Las mascarillas impresas en FDM presentan ciertas sospechas al presentar por la tecnología misma de impresión (sin importar el material) microporos. No obstante tenemos evidencias (estudios con imágenes de microsocopio electrónico de barrido) de que un tratamiento con un polímero o grasa viscosa (por ejemplo vaselina u otra crema de base oleosa) tapa estos microporos. Por otro lado, tratamientos de baño de vapor de acetona en ABS también muestran sellado por microscopía electrónica, aunque este tratamiento es muy peligroso para llevar a cabo en el ámbito doméstico y lo desaconsejamos rotúndamente.

## ¿Qué puedo usar como filtro para mi mascarilla?
No estamos en disposición de certificar ningún material no homologado. Por lo tanto, nos tenemos que remitir a las entidades certificadoras como Eurecat. Hacemos flaco favor en proponer un material u otro en tanto que el grado de filtrado solo se puede medir en laboratorio especializado y hay algo peor que no tener una mascarilla homologada: la falsa sensación de seguridad al usar un material no homologado.

## Me han dicho que el cobre mata al virus, ¿filamento de cobre?
El cobre, como la plata, son catalizadores que inactivan el virus con una vida media de 4 horas en el caso de una superficie de cobre (no filamento dopado con cobre), según un reciente estudio. Vida media significa que en 4 horas el virus reduce su población a la mitad, y en otras 4 horas a la mitad de la mitad, y así. Por lo tanto, el proceso no es inmediato y puede producir falsa sensación de seguridad.

## No tengo ningún filtro de los que me recomendáis, ¿puedo usar esta tela/tejido/papel? Por favor dadme las especificaciones del mismo.
Si no tienes un filtro homologado, lo más sensato es ir a por un buen tapabocas de tela o en su defecto papel0, como el sugerido en https://www.coronavirusmakers.org/index.php/es/impresion-3d/mascarillas/36-diy-mascarilla-con-filtro-de-cafe-en-tan-solo-2-minutos
Lo mejor es tomar todas las precauciones y no exponernos a riesgos en primer lugar. 

De hecho, si tuvieses un filtro homologado pero lo pusieses en una mascarilla impresa en 3D que no garantizase estanqueidad, estarías de nuevo teniendo falsa sensación de seguridad. No tomes riesgos.

## Tengo familiar sanitario y no tienen mascarillas ni viseras o tienen que tirar con una sola, ¿podemos darles algo ya?
No estamos en condiciones de proporcionar mascarillas con garantías para sanitarios en alto riesgo de infección, más allá de un tapabocas de calidad. Trabajamos en ofrecer información a prevención de riesgos laborales como el artículo de la Universidad de Standford:

Can N95 facial masks be used after disinfection? And for how many times? https://stanfordmedicine.app.box.com/v/covid19-PPE-1-2

Donde con evidencia científica se presenta una posible vía de desinfección de una máscara con un tratamiento de calor de 70 ºC por 30 min, en seco, en instalaciones hospitalarias. Los promotores del estudio advierten que BAJO NINGÚN CONCEPTO SE PUEDE HACER EN UN HORNO CASERO. De hacerlo, estamos actuando como vectores de contagio.

## Yo no soy sanitario, pero trabajo por el bien de todos ¿merezco menos que ellos?
Los sanitarios son la primera línea de defensa ante esta pandemia. Cuerpos de seguridad, farmacéuticos, trabajadores de supermercados, transportistas, y un largo etcétera necesitan las mejores medidas de seguridad que podamos suministrarles. Así empezamos a suministrar viseras a cuerpos de seguridad, por ejemplo. Todos los que trabajáis en pleno estado de emergencia merecéis todos nuestra máxima gratitud, respeto y admiración, y trataremos de suministrar cuantas más herramientas de seguridad como podamos.

Visera Libreguard care, con cierre por arriba, algo recomendado por los sanitarios
http://www.libreguard.care
Visera Face Shield by NYU, no muy robusto pero extremadamente sencillo de realizar. DIY
https://open-face-website.now.sh/

## ¿Qué precauciones debemos tomar los que fabriquemos algo asumiendo que podamos contagiar? ¿Fabricación con guantes? ¿Se deben esterilizar/desinfectar los antisalpicaduras, prototipos, etc?
Esto es una preocupación real y muy seria. En general, indicamos que cualquier material debe desinfectarse a su recepción y tenemos un protocolo publicado en XXX(Kermit dixit).

