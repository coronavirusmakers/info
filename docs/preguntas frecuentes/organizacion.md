# Organización
## ¿Cómo os organizaís?
Nos organizamos a través de canales utilizando la herramienta [[https://telegram.org/][Telegram]]. Existe un canal principal llamado [[https://t.me/coronavirus_makers][coronavirus makers]] que es la puerta de entrada. En la parte superior, anclado bajo el nombre del canal encontrarás con enlace con la información actualizada. Existen múltitud canales, organizados por temáticas y regiones. En los temáticos se investiga sobre distintos temas y en los regionales, se organiza la fabricación de piezas a nivel local.

```mermaid
Canal coronavirus makers --> canales temáticos;
Canal coronavirus makers --> canales regionales;
```

## ¿Quienes sois?

## ¿Hay algún diagrama de jerarquía? Por lo menos un diagrama general de quien hay en mando de cosas (admins, coordinadores de zona, logística, producción, etc). Ayudaría a encauzar mejor las cuestiones.

## Soy de Chile, estoy buscando gente que este en este trabajo en mi pais para empezar a colaborar con este proyecto ¿Qué hago?
