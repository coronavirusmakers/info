# Iniciativas internacionales

En esta página recogemos la información sobre todas las iniciativas internacionales relacionadas con CoronavirusMakers.

## Listado de grupos internacionales

Estos son los canales activos a día 27 de Marzo de 2020
Alemania https://t.me/COVID19_MAKERS_ALEMANIA
Andorra https://t.me/CV19_FAB_ANDORRA_COM
Argentina https://t.me/COVID19MAKERSARGENTINA
Chile: https://t.me/covid19makerschile
Colombia: https://t.me/MakersColombia_Cv19
Costa Rica https://t.me/joinchat/LeOScxp4LIgkHasPrlfHUw
Cuba https://t.me/TresDePrintCuba
Ecuador https://t.me/joinchat/QpCdLhsSA2BfXw1IX5cwGA
Guatemala https://t.me/joinchat/QNt7IhgitFFqcGvcklMV4w
Italia (Sardegna) https://t.me/coronavirusmakers_sardegna
México https://t.me/coronavirus_makers_mexico
Panamá https://t.me/panama_makers
Paraguay https://t.me/joinchat/QiSUjBtITsbsuz7juehYwQ 
Perú https://t.me/joinchat/QzCLHRR7nqq99jucbJbA7g
República Dominicana https://t.me/disenadoresayudemosRD
Reino Unido-United Kingdom https://t.me/joinchat/CO9QqRtN7lto9eYZWwMkhA
Suiza https://t.me/Swiss3DMakers
USA https://t.me/coronavirusmakersUSA
Venezuela https://t.me/fabricacionen3dvenezuela

## Cómo poner en marcha esto en mi país

1. En primer lugar verifica que no existe todavía un canal para organizar los esfueros en tu país.
2. Crea un canal en Telegram y difúndelo entre tus grupos o conocidos makers, para comenzar a trabajar en este tema.
3. Entra en el [canal de Telegram de Coronavirus Makers](http://t.me/coronavirus_makers) y menciona a los @admin para que puedan actualizar el directorio
4. Anima a los participantes en el canal a descubrir los avances de los distintos proyectos en la web de [Coronavirus Makers](https://www.coronavirusmakers.org) y el [Foro](https://foro.coronavirusmakers.org).

