# Telegram: Grupo de Makers de Baleares

Aglutina otros dos grupos: de Menorca y de Ibiza

Enlace al grupo: https://t.me/CV_FAB_BAL

Mensaje anclado a fecha 25/03/2020

    🚨IMPORTANTE LEER🚨

    Hola, bienvenid@!
    Este grupo es de fabricación de piezas con impresoras 3D de BALEARES para ayudar a mejorar la situación provocada por el COVID2019.
    El objetivo de este grupo es únicamente gestionar y producir los diseños ya definidos por las diferentes comunidades de diseño, este no es un grupo para diseño de piezas.
    Somos una comunidad de “makers” dedicada únicamente a aspectos relacionados con la producción.

    Ahora que ya formas parte del grupo, y que te quieres implicar en esta acción solidaria, el siguiente paso es calibrar las tolerancias de tu impresora 3D realizando el test de expansión horizontal.
    En este enlace encontraras la información: https://docs.google.com/spreadsheets/d/1g5qdsfdxm6PfHsdv4I1lfgj2vP2y8UQllOk414XNuhg/edit#gid=2057523322

    Si tenéis problemas con el calibrado podéis contactar con @tonilupi para dudas.

    Una vez hayas realizado este calibrado, registra tu impresora 3D en el siguiente formulario. 
    https://forms.gle/TioVzFzw5FqGeGPo8

    📌¿Que imprimo?
    Por ahora nos estamos centrando en imprimir pantallas de protección, pulsa en el link deabajo para conseguir el archivo STL 
    Pieza 3D:
    https://drive.google.com/file/d/1qfS8cqFTOZfPFEvpar7B2qRctb7QatQg/view?usp=sharing

    📌¿Cómo configuro mi impresora 3D?
    • Diámetro del Nozzle: 0.4mm
    • Altura de capa: 0.28mm
    • Relleno: 50%
    • Tipo de relleno: Rejilla
    • Capas superiores: 4
    • Capas inferiores: 2
    • Sin soportes

    📌Ajustes Prusa Slicer:
    https://docs.google.com/spreadsheets/d/1g5qdsfdxm6PfHsdv4I1lfgj2vP2y8UQllOk414XNuhg/edit#gid=2068533048

    📌Una vez impresa la pieza, ¿Qué hago?
    Actualizar el Excel con la producción y Seguir imprimiendo
    🚧Los que hayáis entregado viseras podéis anotar en la columna de "1ª ENTREGA V3.6" o "1ª ENTREGA LOW"  la cantidad que habéis entregado y poner a 0 las columnas de "Nº VISERAS V3.6" y "Nº VISERAS LOW" de forma que en la columna STOCK aparecerán solamente las viseras de nueva fabricación disponibles para su entrega.🚧

    📌No tengo acetatos, ¿Qué hago?
    No te preocupes, otra persona en el grupo se encargará de ensamblar las
    mascaras, sigue imprimiendo partes 3D.

    📌¿Tengo que esterilizar las piezas?
    No hace falta, nosotros las vamos a esterilizar en masa antes de repartirlas. Pero si hay que intentar tener un mínimo de higiene al manipularlas.

    👉Nos pondremos en contacto contigo para venir a buscar las piezas, y repartiremos bobinas de PLA de manera GRATUITA al que le falte material (añadir cuanto material te queda en el excel y marcarlo en rojo si tienes menos de 200g)👈

    Para que te conozcamos mejor, y sepamos tu compromiso necesitamos que rellenes este formulario de bienvenida. 

    https://forms.gle/TioVzFzw5FqGeGPo8

    Gracias! No dudes en consultar al grupo las dudas que tengas. Recuerda no saturarlo por favor.

    enviad un mensaje privado a (@Victor_Gomez) para que podais modificar el excel de la producción y facilitar vuestra dirección de recogida.
    https://docs.google.com/spreadsheets/d/1g5qdsfdxm6PfHsdv4I1lfgj2vP2y8UQllOk414XNuhg/edit?userstoinvite=xavi.256@gmail.com&ts=5e73d3a9&actionButton=1#gid=0 https://docs.google.com/forms/d/e/1FAIpQLSe6z3pPNkQSdbz5xNlG6C6DdhG5JPOkMNd0bruvx18L1v9lVw/viewform?usp=sf_link

    indicarle por privado como estais de filamento a @tresdsoma 

    🤔Si teneis alguna consulta tecnica enviarle privado a @tresdsoma  @FOMA3D
    o a @Makeitor 🤔

    Tenemos 2 grupos mas a parte de este para organizar mejor los participantes de Menorca e Ibiza.
    -MENORCA:https://t.me/joinchat/BVXtUxu_G0YJ6o7nxg3xBQ
    -IBIZA:https://t.me/CV19_FAB_IBI

    ⛔MUY IMPORTANTE⛔
    👉Si estas enfermo o alguien de tu casa esta enfermo parad la producción por seguridad.